package com.ejfirestar.csuci.utils;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;

import org.w3c.dom.Element;

import android.content.Context;

import com.ejfirestar.csuci.R;

/**
 * Collection of useful utility functions.
 * 
 * @author Evan
 */
public class Utils {

    /**
     * @param element the Element in the XML
     * @param tag_name the tag you are searching for in the Element
     * @return the node value of the zeroth index in that Element.
     *         Returns null if not found
     */
    public static String getNodeValue( Element element, String tag_name ) {
        if (element != null && element.getElementsByTagName(tag_name) != null) {
            String temp = element.getElementsByTagName(tag_name).item(0).getChildNodes().item(0)
                    .getNodeValue();
            temp = temp.trim().replaceAll("\\<.*?>", "").replaceAll(" +", " ");
            return temp;
        }
        return null;
    }
    
    public static Card getCard( Context context, String title, String text ) {
        Card card = new Card(context, R.layout.card_inner_layout);
        CardHeader header = new CardHeader(context);
        header.setTitle(title);
        card.addCardHeader(header);
        card.setTitle(text);
        return card;
    }
    

    public static Card getHeaderCard( Context context, String text ) {
        Card card = new Card(context, R.layout.header_inner_layout);
        card.setTitle(text);
        card.setShadow(false);
        card.setBackgroundResourceId(R.drawable.section_header);
        return card;
    }
}