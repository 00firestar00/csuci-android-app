package com.ejfirestar.csuci.rss;


/**
 * Creates a new News Object
 * 
 * @author Evan Jarrett
 */
public class News extends BaseRSS{

    public News( String title, String link ) {
        super(title, "", link);
    }

}