package com.ejfirestar.csuci.rss;


/**
 * Creates a new Event Object
 * 
 * @author Evan Jarrett
 */
public class Event extends BaseRSS {

    public Event( String title, String description, String link ) {
        super(title, description, link);
    }

}