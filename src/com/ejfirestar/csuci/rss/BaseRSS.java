package com.ejfirestar.csuci.rss;

import com.ejfirestar.csuci.R;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import android.content.Context;


/**
 * Creates a new BaseRSS Object for RSS fee Objects
 * 
 * @author Evan Jarrett
 */
public abstract class BaseRSS {

    private String title;
    private String link;
    private String description;

    public BaseRSS( String title, String description, String link ) {
        this.link = link;
        this.title = title;
        this.description = description;
    }

    /**
     * @return the title of the RSS item
     */
    public String getTitle( ) {
        return title;
    }

    /**
     * @param title the title of the RSS item
     */
    public void setTitle( String title ) {
        this.title = title;
    }

    /**
     * @return the link of the RSS item
     */
    public String getLink( ) {
        return link;
    }

    /**
     * @param link the link to the RSS item
     */
    public void setLink( String link ) {
        this.link = link;
    }

    /**
     * @return the description of the RSS item
     */
    public String getDescription( ) {
        return description;
    }

    /**
     * @param description the description of the RSS item to set
     */
    public void setDescription( String description ) {
        this.description = description;
    }
    
    /**
     * @return a new Card for this RSS item
     */
    public Card getCard(Context context) {
        if (description == null) {
            description = ""; // Just in case...
        }
        Card card = new Card(context, R.layout.card_inner_layout);
        CardHeader header = new CardHeader(context);
        header.setTitle(title);
        card.addCardHeader(header);
        card.setTitle(description);
        card.setBackgroundResourceId(R.drawable.card_selector);
        return card;
    }

}