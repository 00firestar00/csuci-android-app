package com.ejfirestar.csuci.faculty;

import com.ejfirestar.csuci.R;

import android.content.Context;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;

/**
 * Creates a new FacultyMember Object
 * 
 * @author Evan Jarrett
 */
public class FacultyMember {

    private String last_name;
    private String first_name;
    private String title;
    private String division;
    private String email;
    private String program;
    private String phone;
    private String fax;
    private String location;
    private String website;

    public FacultyMember( String last_name, String first_name, String title, String division,
            String program, String email, String phone, String fax, String location, String website ) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.email = email;
        this.title = title;
        this.division = division;
        this.program = program;
        this.phone = phone.replaceAll("\\D+","");
        this.fax = fax.replaceAll("\\D+","");
        this.location = location;
        this.website = website;
    }

    /**
     * @return the last name of the FacultyMember
     */
    public String getLastName( ) {
        return last_name;
    }

    /**
     * @param last_name the last name of the FacultyMember to set
     */
    public void setLastName( String last_name ) {
        this.last_name = last_name;
    }

    /**
     * @return the first name of the FacultyMember
     */
    public String getFirstName( ) {
        return first_name;
    }

    /**
     * @param first_name the first name of the FacultyMember to set
     */
    public void setFirstName( String first_name ) {
        this.first_name = first_name;
    }

    /**
     * @return the title of the FacultyMember
     */
    public String getTitle( ) {
        return title;
    }

    /**
     * @param title the title of the FacultyMember to set
     */
    public void setTitle( String title ) {
        this.title = title;
    }

    /**
     * @return the division of the FacultyMember
     */
    public String getDivision( ) {
        return division;
    }

    /**
     * @param division the division of the FacultyMember to set
     */
    public void setDivision( String division ) {
        this.division = division;
    }

    /**
     * @return the program of the FacultyMember
     */
    public String getProgram( ) {
        return program;
    }

    /**
     * @param program the program of the FacultyMember to set
     */
    public void setProgram( String program ) {
        this.program = program;
    }

    /**
     * @return the phone of the FacultyMember
     */
    public String getPhone( ) {
        return phone;
    }

    /**
     * @param phone the phone of the FacultyMember to set
     */
    public void setPhone( String phone ) {
        this.phone = phone.replaceAll("\\D+","");
    }

    /**
     * @return the fax of the FacultyMember
     */
    public String getFax( ) {
        return fax;
    }

    /**
     * @param fax the fax of the FacultyMember to set
     */
    public void setFax( String fax ) {
        this.fax = fax.replaceAll("\\D+","");
    }

    /**
     * @return the location of the FacultyMember
     */
    public String getLocation( ) {
        return location;
    }

    /**
     * @param location the location of the FacultyMember to set
     */
    public void setLocation( String location ) {
        this.location = location;
    }

    /**
     * @return the website of the FacultyMember
     */
    public String getWebsite( ) {
        return website;
    }

    /**
     * @param website the website of the FacultyMember to set
     */
    public void setWebsite( String website ) {
        this.website = website;
    }

    /**
     * @return the email
     */
    public String getEmail( ) {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail( String email ) {
        this.email = email;
    }
    
    /**
     * @return card_array the Array of cards for each attribute.
     */
    public Card[] getCards(Context context) {
        Card[] card_array = new Card[9];
        
        card_array[0] = makeCard(context, "Name", last_name + "," + first_name);
        card_array[1] = makeCard(context, "Title", title);
        card_array[2] = makeCard(context, "Division", division);
        card_array[3] = makeCard(context, "Program", program);
        card_array[4] = makeCard(context, "Email", email);
        card_array[5] = makeCard(context, "Phone", phone);
        card_array[6] = makeCard(context, "Fax", fax);
        card_array[7] = makeCard(context, "Location", location);
        card_array[8] = makeCard(context, "Website", website);
        
        return card_array;
    }
    
    private Card makeCard(Context context, String title, String description) {
        Card card = new Card(context, R.layout.card_inner_layout);
        CardHeader header = new CardHeader(context);
        header.setTitle(title);
        card.addCardHeader(header);
        card.setTitle(description);
        card.setBackgroundResourceId(R.drawable.card_selector);
        return card;
    }
}