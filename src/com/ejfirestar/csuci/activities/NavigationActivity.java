package com.ejfirestar.csuci.activities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.services.HttpService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class NavigationActivity extends Activity {
    
    private GoogleMap map;
    private String marker;
    public static final String ARG_DRAWER_NUMBER = "drawer_array";

    public NavigationActivity() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_map);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);
        Intent intent = getIntent();
        marker = intent.getStringExtra("marker");
        if (marker == null) {
            new AsyncData().execute("http://ejfirestar.com/dev/csuci/api/markers/");
        }
        else {
            marker = marker.replace(" ", "%20");
            new AsyncData().execute("http://ejfirestar.com/dev/csuci/api/markers/search/" + marker);
        }

    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu( Menu menu ) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        return super.onOptionsItemSelected(item);
    }
    
    private class AsyncData extends AsyncTask<String, Integer, String> {

        ArrayList<Location> locations = new ArrayList<Location>();

        protected String doInBackground( String... params ) {
            JSONArray json_array = null;
            try {

                // Making a request to url and getting response
                json_array = HttpService.GET(params[0]);
                for (int i = 0; i < json_array.length(); i++) {
                    JSONObject result = json_array.getJSONObject(i);
                    
                    locations.add(new Location(result.getDouble("latitude"),
                            result.getDouble("longitude"),result.getString("title")));

                }
            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate( Integer... progress ) {

        }

        protected void onPostExecute( String result ) {
            for (Location location : locations) {
                map.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.longitude))
                .title(location.title));
            }
        }

    }
    
    private class Location {
        public double latitude;
        public double longitude;
        public String title;
        
        public Location( double latitude, double longitude, String title) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.title = title;
        }
    }

}