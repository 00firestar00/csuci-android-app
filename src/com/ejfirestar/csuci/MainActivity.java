package com.ejfirestar.csuci;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ejfirestar.csuci.activities.NavigationActivity;
import com.ejfirestar.csuci.fragments.BookstoreFragment;
import com.ejfirestar.csuci.fragments.DiningFragment;
import com.ejfirestar.csuci.fragments.DirectoryFragment;
import com.ejfirestar.csuci.fragments.HomeFragment;
import com.ejfirestar.csuci.fragments.LibraryFragment;

/**
 * CSUCI App
 * This is a Capstone Project as well as a proof of concept application.
 * This app is designed to give the users information about the school, in a convenient form factor.
 * @author Evan Jarrett
 * @version 0.1
 */
public class MainActivity extends Activity {
    private DrawerLayout drawer_layouts;
    private ListView drawer_list;
    private ActionBarDrawerToggle drawer_toggle;

    private CharSequence drawer_title;
    private CharSequence title;
    private String[] drawer_items;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    
        title = drawer_title = getTitle();
        drawer_items = getResources().getStringArray(R.array.drawer_array);
        drawer_layouts = ( DrawerLayout ) findViewById(R.id.drawer_layout);
        drawer_list = ( ListView ) findViewById(R.id.left_drawer);
    
        // set a custom shadow that overlays the main content when the drawer opens
        drawer_layouts.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        drawer_list.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item,
                drawer_items));
        drawer_list.setOnItemClickListener(new DrawerItemClickListener());
    
        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        drawer_toggle = new ActionBarDrawerToggle(this, /* host Activity */
        drawer_layouts, /* DrawerLayout object */
        R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
        R.string.drawer_open, /* "open drawer" description for accessibility */
        R.string.drawer_close /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed( View view ) {
                getActionBar().setTitle(title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
    
            public void onDrawerOpened( View drawerView ) {
                getActionBar().setTitle(drawer_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawer_layouts.setDrawerListener(drawer_toggle);
    
        if (savedInstanceState == null) {
            selectItem(9001);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    
    @Override
    protected void onPostCreate( Bundle savedInstanceState ) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawer_toggle.syncState();
    }

    @Override
    public void onBackPressed( ) {
        FragmentManager fragment_manager = getFragmentManager();
        fragment_manager.popBackStack();
        if (fragment_manager.getBackStackEntryCount() == 0) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu( Menu menu ) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (drawer_toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle( CharSequence title ) {
        this.title = title;
        getActionBar().setTitle(title);
    }

    @Override
    public void onConfigurationChanged( Configuration newConfig ) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawer_toggle.onConfigurationChanged(newConfig);
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
            selectItem(position);
        }
    }

    private void selectItem( int position ) {
        // update the main content by replacing fragments
        Bundle args = new Bundle();
        if (position < drawer_items.length) {
            args.putInt(HomeFragment.ARG_DRAWER_NUMBER, position);
        }
        else {
            args.putInt(HomeFragment.ARG_DRAWER_NUMBER, 0);
        }
    
        FragmentManager fragmentManager = getFragmentManager();
    
        Fragment fragment = null;
    
        switch (position) {
        case 0:
            fragment = new HomeFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
            .addToBackStack(null).commit();
        break;
        case 1:
            fragment = new DirectoryFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null).commit();
        break;
        case 2:
            Intent intent = new Intent(this, NavigationActivity.class);
            startActivity(intent);
        break;
        case 3:
            fragment = new DiningFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null).commit();
        break;
        case 4:
            fragment = new LibraryFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null).commit();
        break;
        case 5:
            fragment = new BookstoreFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null).commit();
        break;
        default:
            fragment = new HomeFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            position = 0;
        break;
        }
        // update selected item and title, then close the drawer
        drawer_list.setItemChecked(position, true);
        setTitle(drawer_items[position]);
        drawer_layouts.closeDrawer(drawer_list);
    }
}