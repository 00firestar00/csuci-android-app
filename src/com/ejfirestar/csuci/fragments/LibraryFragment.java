package com.ejfirestar.csuci.fragments;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.activities.NavigationActivity;
import com.ejfirestar.csuci.utils.Utils;

/**
 * Fragment that appears in the "content_frame"
 */
public class LibraryFragment extends Fragment{

    public static final String ARG_DRAWER_NUMBER = "drawer_array";
    private View root_view;

    ArrayList<String> link_list = new ArrayList<String>();

    public LibraryFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {

        root_view = inflater.inflate(R.layout.card_fragment, container, false);
        CardArrayAdapter adapter = new CardArrayAdapter(getActivity(), new ArrayList<Card>());
        CardListView cards_list = ( CardListView ) root_view.findViewById(R.id.myList);
        cards_list.setAdapter(adapter);

        String[] library_info = getResources().getStringArray(R.array.library_info);
        int number = getArguments().getInt(ARG_DRAWER_NUMBER);
        String item = getResources().getStringArray(R.array.drawer_array)[number];
        getActivity().setTitle(item);
       

        cards_list.setAdapter(adapter);

        //adapter.add(getCard("Library"));
        Card card1 = Utils.getCard(root_view.getContext(), "Location", library_info[0]);
        card1.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                Intent intent = new Intent(getActivity(), NavigationActivity.class);
                intent.putExtra("marker", "Library");
                startActivity(intent);
            }
        });
        adapter.add(card1);
        
        adapter.add(Utils.getCard(root_view.getContext(), "Library Hours", library_info[1]));
        
        Card card2 = Utils.getCard(root_view.getContext(), "Website","http://library.csuci.edu/");
        card2.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://library.csuci.edu/"));
                startActivity(browserIntent);
            }
        });
        adapter.add(card2);
        
        return root_view;
    }
}