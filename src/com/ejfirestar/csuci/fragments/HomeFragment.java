package com.ejfirestar.csuci.fragments;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.rss.BaseRSS;
import com.ejfirestar.csuci.rss.Event;
import com.ejfirestar.csuci.rss.News;
import com.ejfirestar.csuci.services.HttpService;
import com.ejfirestar.csuci.utils.Utils;

/**
 * Fragment that appears in the "content_frame"
 */
public class HomeFragment extends Fragment {

    public static final String ARG_DRAWER_NUMBER = "drawer_array";
    private View root_view;

    public HomeFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {
        int i = getArguments().getInt(ARG_DRAWER_NUMBER);
        String item = getResources().getStringArray(R.array.drawer_array)[i];
        getActivity().setTitle(item);
        root_view = inflater.inflate(R.layout.card_fragment, container, false);
        new AsyncData().execute("http://www.csuci.edu/news/rss.xml",
                "http://ciapps.csuci.edu/events/events.xml");
        return root_view;
    }

    private class AsyncData extends AsyncTask<String, Integer, Boolean> {

        ArrayList<News> news_list = new ArrayList<News>();
        ArrayList<Event> events_list = new ArrayList<Event>();

        protected Boolean doInBackground( String... params ) {
            // Making a request to url and getting response
            NodeList news_nodes = HttpService.getXML(params[0]);
            NodeList event_nodes = HttpService.getXML(params[1]);

            if (news_nodes != null && event_nodes != null) {
                for (int i = 0; i < 5; i++) { // Shouldn't need more then 5. Web site only shows latest 4.
                    Node node = news_nodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = ( Element ) node;
                        String title = Utils.getNodeValue(element, "title");
                        String link = Utils.getNodeValue(element, "link");
                        News news_item = new News(title, link);
                        news_list.add(news_item);
                    }
                }
                for (int i = 0; i < 10; i++) { // Shouldn't need more then 10. Web site only shows latest 4.
                    Node node = event_nodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = ( Element ) node;
                        String title = Utils.getNodeValue(element, "title");
                        String description = Utils.getNodeValue(element, "description");
                        String link = Utils.getNodeValue(element, "link");
                        Event event_item = new Event(title, description, link);
                        events_list.add(event_item);
                    }
                }
                return true;
            }
            return false;
        }

        protected void onProgressUpdate( Integer... progress ) {

        }

        protected void onPostExecute( Boolean result ) {
            CardArrayAdapter adapter = new CardArrayAdapter(root_view.getContext(), new ArrayList<Card>());
            CardListView cards_list = ( CardListView ) root_view.findViewById(R.id.myList);
            cards_list.setAdapter(adapter);

            if (result) {
                adapter.add(Utils.getHeaderCard(root_view.getContext(), "News"));
                adapter.addAll(getCards(news_list));
                adapter.add(Utils.getHeaderCard(root_view.getContext(), "Upcoming Events"));
                adapter.addAll(getCards(events_list));
            }
            else {
                adapter.add(Utils.getHeaderCard(root_view.getContext(), "Failed to retrieve News and Events.\nNo Internet connection."));
                Toast.makeText(getActivity(), R.string.home_error, Toast.LENGTH_LONG).show();
            }
        }
    }

    private ArrayList<Card> getCards( ArrayList<? extends BaseRSS> list ) {
        ArrayList<Card> cards = new ArrayList<Card>();
        for (BaseRSS item : list) {
            Card event_card = item.getCard(root_view.getContext());
            final String link = item.getLink();
            event_card.setOnClickListener(new Card.OnCardClickListener() {
                @Override
                public void onClick( Card card, View view ) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);
                }
            });
            cards.add(event_card);
        }
        return cards;
    }

}