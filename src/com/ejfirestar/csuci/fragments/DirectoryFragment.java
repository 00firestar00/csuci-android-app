package com.ejfirestar.csuci.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.services.HttpService;

/**
 * Fragment that appears in the "content_frame"
 */
public class DirectoryFragment extends ListFragment {
    public static final String ARG_DRAWER_NUMBER = "drawer_array";

    public DirectoryFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        handleIntent(getActivity().getIntent());
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {
        int i = getArguments().getInt(ARG_DRAWER_NUMBER);
        String item = getResources().getStringArray(R.array.drawer_array)[i];
        getActivity().setTitle(item);

        View root_view = inflater.inflate(R.layout.fragment_directory, container, false);

        new AsyncData().execute("http://ejfirestar.com/dev/csuci/api/faculty/");
        return root_view;
    }

    @Override
    public void onListItemClick( ListView l, View v, int position, long id ) {
        FragmentManager fragmentManager = getFragmentManager();
        super.onListItemClick(l, v, position, id);
        String selection = l.getItemAtPosition(position).toString().replaceAll(" ", "%20");
        Fragment fragment = DirectoryInfoFragment.newInstance(selection);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
            .addToBackStack(null).commit();
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
        inflater.inflate(R.menu.directory_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = ( SearchManager ) getActivity().getSystemService(
                Context.SEARCH_SERVICE);
        SearchView searchView = ( SearchView ) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity()
                .getComponentName()));
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
        case R.id.search:
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Handling intent data
     */
    private void handleIntent( Intent intent ) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            Toast.makeText(getActivity().getApplicationContext(), query, Toast.LENGTH_LONG).show();
        }

    }

    private class AsyncData extends AsyncTask<String, Integer, Boolean> {

        ArrayList<String> faculty_list = new ArrayList<String>();

        protected Boolean doInBackground( String... params ) {
            JSONArray json_array = null;
            // Making a request to url and getting response
            json_array = HttpService.GET(params[0]);
            if (json_array != null) {
                try {
                    for (int i = 0; i < json_array.length(); i++) {
                        JSONObject result = json_array.getJSONObject(i);
                        faculty_list.add(result.getString("last_name") + ','
                                + result.getString("first_name"));
                    }
                }
                catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return true;
            }
            else {
                return false;
            }
        }

        protected void onProgressUpdate( Integer... progress ) {

        }

        protected void onPostExecute( Boolean result ) {
            View root_view = DirectoryFragment.this.getListView();
            ListView list_view = ( ListView ) root_view.findViewById(R.id.listview);
            if (result) {
                list_view.setAdapter(new ArrayAdapter<String>(root_view.getContext(),
                        android.R.layout.simple_list_item_1, faculty_list));
            }
            else {
                Toast.makeText(getActivity(), R.string.directory_error, Toast.LENGTH_LONG).show();
            }
        }

    }

}