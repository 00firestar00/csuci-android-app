package com.ejfirestar.csuci.fragments;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.utils.Utils;

/**
 * Fragment that appears in the "content_frame"
 */
public class DiningFragment extends Fragment {

    public static final String ARG_DRAWER_NUMBER = "drawer_array";
    private View root_view;

    ArrayList<String> link_list = new ArrayList<String>();

    public DiningFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {

        root_view =  inflater.inflate(R.layout.card_fragment, container, false);
        CardArrayAdapter adapter = new CardArrayAdapter(getActivity(), new ArrayList<Card>());
        CardListView cards_list = ( CardListView ) root_view.findViewById(R.id.myList);
        cards_list.setAdapter(adapter);

        int number = getArguments().getInt(ARG_DRAWER_NUMBER);
        String item = getResources().getStringArray(R.array.drawer_array)[number];
        getActivity().setTitle(item);
       
        String[] dining_names = getResources().getStringArray(R.array.dining_names);
        String[] dining_hours = getResources().getStringArray(R.array.dining_hours);
        
        //Same amount of titles as hours... hopefully
        if (dining_names.length == dining_hours.length) {
            for (int i = 0; i < dining_hours.length; i++) {
                adapter.add(Utils.getCard(root_view.getContext(), dining_names[i], dining_hours[i]));
            }
        }

        return root_view;
    }

}