package com.ejfirestar.csuci.fragments;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.activities.NavigationActivity;
import com.ejfirestar.csuci.faculty.FacultyMember;
import com.ejfirestar.csuci.services.HttpService;

/**
 * Fragment that appears in the "content_frame"
 */
public class DirectoryInfoFragment extends Fragment {
    
    private View root_view;
    
    public DirectoryInfoFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static DirectoryInfoFragment newInstance( String name ) {
        DirectoryInfoFragment fragment = new DirectoryInfoFragment();
        Bundle args = new Bundle();
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {
        String[] name = getArguments().getString("name").split(",");
        getActivity().setTitle(name[0].replace("%20", " "));
        getActivity().setTitle(name[1].replace("%20", " "));
        root_view = inflater.inflate(R.layout.card_fragment, container, false);

        new AsyncData().execute("http://ejfirestar.com/dev/csuci/api/faculty/search/" + name[0] + '/' + name[1]);
        return root_view;
    }

    private class AsyncData extends AsyncTask<String, Integer, String> {

        private FacultyMember faculty_member;

        protected String doInBackground( String... params ) {
            JSONArray json_array = null;
            try {
                // Making a request to url and getting response
                json_array = HttpService.GET(params[0]);
                JSONObject result = json_array.getJSONObject(0);

                faculty_member = new FacultyMember(
                    result.getString("last_name"),
                    result.getString("first_name"),
                    result.getString("title"),
                    result.getString("division"),
                    result.getString("program"),
                    result.getString("email"),
                    result.getString("phone"),
                    result.getString("fax"),
                    result.getString("location"),
                    result.getString("website") );
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate( Integer... progress ) {

        }

        protected void onPostExecute( String result ) {
            CardArrayAdapter adapter = new CardArrayAdapter(root_view.getContext(), new ArrayList<Card>());
            CardListView cards_list = ( CardListView ) root_view.findViewById(R.id.myList);
            cards_list.setAdapter(adapter);
            int i = 0;
            for (Card card : faculty_member.getCards(root_view.getContext())) {
                card.setOnClickListener(getOnClickListener(i));
                adapter.add(card);
                i++;
            }
        }
        
        private Card.OnCardClickListener getOnClickListener(int index) {
            switch(index) {
            case 4:
                return new Card.OnCardClickListener() {
                    @Override
                    public void onClick( Card card, View view ) {
                        Intent email_intent = new Intent(Intent.ACTION_SEND);
                        email_intent.putExtra(Intent.EXTRA_EMAIL, new String[] {faculty_member.getEmail()});
                        email_intent.setType("plain/text");
                        startActivity(email_intent);
                    }
                };
            case 5:
                return new Card.OnCardClickListener() {
                    @Override
                    public void onClick( Card card, View view ) {
                        Intent phone_intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + faculty_member.getPhone()));
                        startActivity(phone_intent);
                    }
                };
            case 6:
                return new Card.OnCardClickListener() {
                    @Override
                    public void onClick( Card card, View view ) {
                        Intent fax_intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + faculty_member.getFax()));
                        startActivity(fax_intent);
                    }
                };
            case 7:
                return new Card.OnCardClickListener() {
                    @Override
                    public void onClick( Card card, View view ) {
                        Intent intent = new Intent(getActivity(), NavigationActivity.class);
                        intent.putExtra("marker", faculty_member.getLocation());
                        startActivity(intent);
                    }
                };
            case 8:
                final String website = faculty_member.getWebsite();
                if (website != null || website != "") {
                    return new Card.OnCardClickListener() {
                        @Override
                        public void onClick( Card card, View view ) {
                            Intent browser_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                            startActivity(browser_intent);
                        }
                    };
                }
            default:
                return null;
            }
        }
    }
}