package com.ejfirestar.csuci.fragments;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ejfirestar.csuci.R;
import com.ejfirestar.csuci.activities.NavigationActivity;
import com.ejfirestar.csuci.utils.Utils;

/**
 * Fragment that appears in the "content_frame"
 */
public class BookstoreFragment extends Fragment {

    public static final String ARG_DRAWER_NUMBER = "drawer_array";
    private View root_view;

    ArrayList<String> link_list = new ArrayList<String>();

    public BookstoreFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState ) {

        root_view = inflater.inflate(R.layout.card_fragment, container, false);
        CardArrayAdapter adapter = new CardArrayAdapter(root_view.getContext(), new ArrayList<Card>());
        CardListView cards_list = ( CardListView ) root_view.findViewById(R.id.myList);
        cards_list.setAdapter(adapter);

        String[] bookstore_titles = getResources().getStringArray(R.array.bookstore_titles);
        final String[] bookstore_info = getResources().getStringArray(R.array.bookstore_info);
        int number = getArguments().getInt(ARG_DRAWER_NUMBER);
        String item = getResources().getStringArray(R.array.drawer_array)[number];
        getActivity().setTitle(item);
        
        //adapter.add(new CardHeader("Bookstore"));
        Card card0 = Utils.getCard(root_view.getContext(), bookstore_titles[0], bookstore_info[0]);
        card0.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                Intent intent = new Intent(getActivity(), NavigationActivity.class);
                intent.putExtra("marker", "Bookstore");
                startActivity(intent);;
            }
        });
        adapter.add(card0);

        adapter.add( Utils.getCard(root_view.getContext(), bookstore_titles[1], bookstore_info[1]));

        Card card2 = Utils.getCard(root_view.getContext(), bookstore_titles[2], bookstore_info[2]);
        card2.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                String phone_number = bookstore_info[2].replaceAll("\\D+", "");
                Intent phone_intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                        + phone_number));
                startActivity(phone_intent);
            }
        });
        adapter.add(card2);

        Card card3 = Utils.getCard(root_view.getContext(), bookstore_titles[3], bookstore_info[3]);
        card3.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                String fax_number = bookstore_info[3].replaceAll("\\D+", "");
                Intent fax_intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                        + fax_number));
                startActivity(fax_intent);
            }
        });
        adapter.add(card3);

        Card card4 = Utils.getCard(root_view.getContext(), bookstore_titles[4], bookstore_info[4]);
        card4.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                Intent email_intent = new Intent(Intent.ACTION_SEND);
                email_intent.putExtra(Intent.EXTRA_EMAIL, bookstore_info[4]);
                email_intent.setType("plain/text");
                startActivity(email_intent);
            }
        });
        adapter.add(card4);
        
        Card card5 = Utils.getCard(root_view.getContext(), "Website", "http://www.bkstr.com/Home/10001-10978-1");
        card5.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick( Card card, View view ) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.bkstr.com/Home/10001-10978-1"));
                startActivity(browserIntent);
            }
        });
        adapter.add(card5);

        return root_view;
    }
}