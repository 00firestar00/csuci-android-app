<?php

// Use Loader() to autoload our model
$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
    __DIR__ . '/models/'
))->register();

$di = new \Phalcon\DI\FactoryDefault();

//Set up the database service
$di->set('db', function(){
    return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        'host'     => 'localhost',
        'login'    => '',
        'password' => '',
        'dbname'   => ''
    ));
});

//Create and bind the DI to the application
$app = new \Phalcon\Mvc\Micro($di);

//Retrieves all faculty
$app->get('/api/faculty', function() use ($app) {

    $phql = "SELECT id,first_name,last_name FROM faculty ORDER BY last_name";
    $faculty = $app->modelsManager->executeQuery($phql);

    $data = array();
    foreach ($faculty as $member) {
        $data[] = array(
            'id'   => $member->id,
            'last_name' => $member->last_name,
            'first_name' => $member->first_name
        );
    }

    echo json_encode($data);
});

//Searches for faculty with $name in their name
$app->get('/api/faculty/search/{last_name}/{first_name}', function($last_name, $first_name) use ($app) {

    $phql = "SELECT * FROM faculty WHERE first_name LIKE :first_name: AND last_name LIKE :last_name:";
    $faculty = $app->modelsManager->executeQuery($phql, array(
        'last_name' => '%' . $last_name . '%',
        'first_name' => '%' . $first_name . '%'
    ));

    $data = array();
    foreach ($faculty as $member) {
        $data[] = array(
            'id'          => $member->id,
            'first_name'  => $member->first_name,
            'last_name'   => $member->last_name,
            'title'       => $member->title,
            'affiliation' => $member->affiliation,
            'email'       => $member->email,
            'pod_name'    => $member->pod_name,
            'program'     => $member->program,
            'division'    => $member->division,
            'phone'       => $member->phone,
            'fax'         => $member->fax,
            'location'    => $member->location,
            'website'     => $member->website
        );
    }

    echo json_encode($data);

});

//Retrieves faculty based on primary key
$app->get('/api/faculty/{id:[0-9]+}', function($id) use ($app) {

    $phql = "SELECT * FROM faculty WHERE id = :id:";
    $member = $app->modelsManager->executeQuery($phql, array(
        'id' => $id
    ))->getFirst();

    //Create a response
    $response = new Phalcon\Http\Response();

    if ($member == false) {
        $response->setJsonContent(array('status' => 'NOT-FOUND'));
    } else {
        $response->setJsonContent(array(
            'status' => 'FOUND',
            'data' => array(
                'id' => $member->id,
                'last_name'  => $member->last_name,
                'first_name' => $member->first_name
            )
        ));
    }

    return $response;
});

//Retrieves all markers
$app->get('/api/markers', function() use ($app) {

    $phql = "SELECT * FROM markers ORDER BY title";
    $markers = $app->modelsManager->executeQuery($phql);

    $data = array();
    foreach ($markers as $marker) {
        $data[] = array(
            'id'   => $marker->id,
            'title' => $marker->title,
            'longitude' => $marker->longitude,
            'latitude' => $marker->latitude
        );
    }

    echo json_encode($data);
});



$app->handle();
?>
