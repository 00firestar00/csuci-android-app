<?php

use Phalcon\Mvc\Model;

class faculty extends Model
{

    public $id;
    public $first_name;
    public $last_name;
    public $title;
    public $affiliation;
    public $email;
    public $pod_name;
    public $program;
    public $division;
    public $phone;
    public $fax;
    public $location;
    public $website;

    public function getSource()
    {
        return 'faculty';
    }

}
?>
