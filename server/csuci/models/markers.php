<?php

use Phalcon\Mvc\Model;

class markers extends Model
{

    public $id;
    public $longitude;
    public $latitude;
    public $title;

    public function getSource()
    {
        return 'markers';
    }

}
?>
