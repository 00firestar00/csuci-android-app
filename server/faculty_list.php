<?php
  $db_host  = 'localhost';
  $db_name  = '';
  $db_user  = '';
  $db_pass  = '';
  $table_name = ''; 

  $base_url = 'http://ciapps.csuci.edu/directory/Search?q=Faculty&filters=Faculty&pageTarget=results&page=';
  $details_url = 'http://ciapps.csuci.edu/directory/Details/';
  $faculty_list = array();
  $debug = TRUE;

  function debug($string) {
    global $debug;

    if ($debug) {
      echo $string;
    }
  }

  function get_sql_connection($con) {
    global $db_name;

    if($con == FALSE) {
      die('<p class="fail">Cannot connect to database' . mysql_error() . '</p>');
      return FALSE;
    } 
    else {
      debug('<p class="success">Connected to database</p>');
      mysql_select_db($db_name, $con);
      return TRUE;
    }
  }  

  function get_headers_do_not_use($url) {
    $dom = new DOMDocument();
    $html = curl_load($url);
    $dom->validateOnParse = TRUE; //<!-- this first
    $dom->loadHTML($html);        //'cause 'load' == 'parse
    $dom->preserveWhiteSpace = FALSE;
    $th_list = $dom->getElementsByTagName('th');
    $list = array();
    foreach($th_list as $th) {
      $list[] = $th->nodeValue;
    }
    return $list;
  }

  function split_name($list) {
    $name = $list[0];
    $last_name = strtok($name,',');
    $first_name = strtok(',');
    $new_list[0] = $first_name;
    $new_list[1] = $last_name;
    for($i = 1; $i < count($list); $i++) {
      $new_list[] = $list[$i];
    }
    return $new_list;
  }

  function to_database($list, $con) {
    global $table_name;

    // YES I KNOW THERE IS A BETTER WAY TO DO THIS
    $query = 'INSERT INTO ' . $table_name . '(first_name,last_name,title,affiliation,email,pod_name,program,division,phone,fax,location,website) values (';
    // regex for phones just cuz: \(\d{3}\)\s\d{3}\-\d{4}
    if (sizeof($list) == 7) {
      $query = 'INSERT INTO ' . $table_name . '(first_name,last_name,title,affiliation,email,pod_name,program) values (';
    }
    for($count = 0; $count < sizeof($list); $count++) {
      if( $count < 12) {
        $query = $query . "'" . $list[$count] . "',";
      }
    }
    $query = substr_replace($query ,"",-1) . ');';
    echo $query;
    if (!mysql_query($query, $con)) {
      debug('<p class="fail">Error: ' . mysql_error() . '</p>');
      return FALSE;
    }

    debug('<p class="success">1 record added</p>');
    return TRUE;
  }

  function put_usernames($item, $con) {
    $query = 'INSERT INTO usernames(username) VALUES (';
    $query = $query . "'" . $item . "');";
//    $query = substr_replace($query ,"",-1) . ');';
    echo $query;
    if (!mysql_query($query, $con)) {
      debug('<p class="fail">Error: ' . mysql_error() . '</p>');
      return FALSE;
    }

    debug('<p class="success">1 record added</p>');
    return TRUE;
  }

  function curl_load($url){
    curl_setopt($ch=curl_init(), CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  function get_names($page_number = 1 ) {
    global $base_url, $faculty_list, $con;
    $url = $base_url . strval($page_number);
    $dom = new DOMDocument();
    $html = curl_load($url);
    $dom->validateOnParse = TRUE; //<!-- this first
    $dom->loadHTML($html);        //'cause 'load' == 'parse
    $dom->preserveWhiteSpace = FALSE;
    $a_list = $dom->getElementsByTagName('a');
    
    foreach ($a_list as $a) {
      $a = preg_replace('!\s+!', ' ', $a->getAttribute('href'));
      if ( substr($a,0,19) === '/directory/Details/' ) {
         $a = substr($a,19,strlen($a));
	 $a = strtok($a,'?');
         $faculty_list[] = $a;
         put_usernames($a, $con);
      }
    }
    debug('<p class="success">Got Names Page: ' . $page_number . '</p>');
    return TRUE;
  }

  function get_usernames($con) {
    $query = 'SELECT * FROM usernames';
    if (!($result = mysql_query($query, $con)) ) {
      die('<p class="fail">Error: ' . mysql_error() . '</p>');
      return FALSE;
    }
    $list = array();
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
       $list[$row['id']-1] = $row['username'];
    }
    debug('<p class="success">records received</p>');
    return $list;
  }

  function get_details($url) {
    $dom = new DOMDocument();
    $html = curl_load($url);
    $dom->validateOnParse = TRUE; //<!-- this first
    $dom->loadHTML($html);        //'cause 'load' == 'parse
    $dom->preserveWhiteSpace = FALSE;
    $td_list = $dom->getElementsByTagName('td');
    $list = array();
    foreach($td_list as $td) {
      $temp = $td->nodeValue;
      $temp = str_replace('\n','', $temp);
      $temp = preg_replace('!\s+!', ' ', $temp);
      $temp = preg_replace("/[^\x01-\x7F]/","", $temp);
      $list[] = $temp;
    }
    debug('<p class="success">Got Details: ' . $list[0] . '</p>');
    return $list;
  }

  $con = mysql_connect($db_host, $db_user, $db_pass);
  if (get_sql_connection($con)) {
    // for ($i = 1; $i < 11; $i++) {
    //   get_names($i);
    // }
    $faculty_list = get_usernames($con);
    foreach ($faculty_list as $faculty) {
      $faculty_url = $details_url . $faculty;
      to_database(split_name(get_details($faculty_url)), $con);
      echo $i++;
    }

    mysql_close($con);
  }

?>
