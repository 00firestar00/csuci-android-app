CSUCI Android Application
=====================
While most of the information will be updated in the [Design Document](https://docs.google.com/document/d/15Ek8T910zrbJWuBkKURyo1bB8zpE-wFA-tSmkddcOn4/edit), this will provide a brief overview of my process and outline.

----------
The Project consists of both a server sided database and API, as well as an Android App that will run on clients mobile devices.

##Server Side##

This portion of the code will consist of a MySQL database along with a PHP RESTful API.
You can find all the code for this portion of the project in the */server* directory. The reason for this is to provide myself with a quick and easy way to get entries from my database without having to have a direct connection to it. The results are displayed as json format and can be easily parsed.

##Android App##

This is where most of the work is. The purpose of this project is to demonstrate my knowledge of Java, Android, as well as the various other things needed for this project to work.
The main feautures include:

* News and Events feed on homescreen (parsed from the RSS feeds on the school website)
* Bookstore, Library, and UGlen Dining information along with hours of operations, websites, phone numbers and navigation via the map
* Map with markers for buildings,classrooms etc.
* Full teachers directory with links to email, website, phone and office location (via map navigation)

##Dependencies##

There are a few dependencies needed in order to build this project. I have tried to minimize this, and will try and reduce the amount of dependencies in the future.

* [cardslib](https://github.com/gabrielemariotti/cardslib)
    * Used for the card layout views. Great library, and fairly easy to use. I had to create my own layouts to make it work for my needs, but its very open and customizable
* [Google Play Services Lib](http://developer.android.com/google/play-services/setup.html)
    * This is required for the mapping functionality of the App. This should already be downloaded when you setup the Android SDK. But if you are unsure how to use it, check out this link.
* [Phalcon](http://phalconphp.com/en/)
    * This is the PHP framework that I used to create the API. If you want to follow the tutorial for creating an API, take a look [here](http://docs.phalconphp.com/en/latest/reference/tutorial-rest.html#defining-the-api).

##Tasks and Issues##

For a more updated list, be sure to check the [issue tracker](https://bitbucket.org/00firestar00/csuci-android-app/issues).

Some primary issues I have been having include:

* Searching for faculty in the directory and specific buildings in the lists.
* Not all classrooms and buildings are displayed on the map. Still need to populate the database with nearly 250 entries for faculty offices alone.
* Map should provide route information to classrooms if possible (have not researched this yet).
* App currently only supports Android 4.X devices. Should add backwards compatibility with 2.X devices.
* Needs different layouts for larger devices. Layouts not optimized for tablets.
